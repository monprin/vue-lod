FROM node:13-buster

RUN apt update && \
  apt install -y vim git

RUN npm i -g yarn

RUN mkdir /app

WORKDIR /app

CMD /usr/local/bin/yarn install && /usr/local/bin/yarn serve