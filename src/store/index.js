import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: [],
    zombieStatus: false
  },
  getters: {
    getStatusMsgs(state) {
      return state.status;
    }
  },
  mutations: {
    addStatusMsg(state, payload) {
      state.status.push(payload);
    },
    zombieStatusMsg(state) {
      state.zombieStatus = true;
    },
    clearStatusMsgs(state) {
      if (state.zombieStatus && state.status.length > 0) {
        state.status = [];
      }
      state.zombieStatus = false;
    }
  },
  modules: {}
});
