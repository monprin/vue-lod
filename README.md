# vue-lod

## Build and Start with Docker

```
./run-vue.sh
```

# Composition API

https://vue-composition-api-rfc.netlify.com/

https://vue-composition-api-rfc.netlify.com/api.html

https://github.com/LinusBorg/composition-api-demos

https://github.com/LinusBorg/talks/tree/master/2019-10-04%20Vuejs%20London

# Normal Stuff

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Run your end-to-end tests

```
yarn test:e2e
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
